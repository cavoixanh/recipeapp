//
//  Thread.swift
//  Travid
//
//  Created by Tuấn Bờm on 6/10/18.
//  Copyright © 2018 Tuấn Bờm. All rights reserved.
//

import Foundation

final class Thread {
	
	private init() {
	}
	
	class func main(_ action: @escaping (() -> Void)) {
        DispatchQueue.main.async {
            action()
        }
	}
    
    class func mainAfter(_ delay: Double = 0.002, _ action: @escaping (() -> Void)) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
            action()
        })
    }
    
    class func background(_ action: @escaping (() -> Void)) {
        DispatchQueue.global(qos: .background).async {
            action()
        }
    }
    
    class func backgroundAfter(_ delay: Double = 0.001, _ action: @escaping (() -> Void)) {
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + delay) {
            action()
        }
    }
	
}
