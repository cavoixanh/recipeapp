//
//  CreateRecipeVC.swift
//  RecipeApp
//
//  Created by Admin on 2/22/20.
//  Copyright © 2020 recipe. All rights reserved.
//

import UIKit

class CreateRecipeVC: UIViewController {

    var presenter = CreateRecipepresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigation()
        configureTableView()
        // Do any additional setup after loading the view.
    }
    
    private func configureNavigation() {
        let saveButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveRecipe(_:)))
        navigationItem.rightBarButtonItems = [saveButton]
    }
    
    private func configureTableView() {
        //tableView.delegate = self
    }
    
    @objc private func saveRecipe(_ sender: UIBarButtonItem) {
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

