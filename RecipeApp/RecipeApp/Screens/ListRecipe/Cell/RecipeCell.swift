//
//  RecipeCell.swift
//  RecipeApp
//
//  Created by Admin on 2/22/20.
//  Copyright © 2020 recipe. All rights reserved.
//

import UIKit
import SwiftyAttributes

class RecipeCell: UITableViewCell {
    static var fileNameCell = "RecipeCell"
    @IBOutlet private weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupCell(model: RecipeModel) {
        let steps = model.content.components(separatedBy: "///")
        let attribute = NSMutableAttributedString()
        attribute.append(model.name.withFont(UIFont.boldSystemFont(ofSize: 16)))
    
        for i in 0..<steps.count {
            let step = steps[i]
            attribute.append("STEP \(i): \n".withFont(UIFont.boldSystemFont(ofSize: 14)))
            attribute.append(setupAttribute(step: step))
        }
        
        contentLabel.attributedText = attribute
    }
    
    private func setupAttribute(step: String) -> NSAttributedString {
        return "\(step)\n".withFont(UIFont.systemFont(ofSize: 14))
    }
    
}
