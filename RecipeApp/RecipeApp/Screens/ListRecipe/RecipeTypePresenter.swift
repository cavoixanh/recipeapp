//
//  RecipeTypePresenter.swift
//  RecipeApp
//
//  Created by Admin on 2/21/20.
//  Copyright © 2020 recipe. All rights reserved.
//

import UIKit
import AEXML
import RealmSwift
import Realm

final class RecipeTypePresenter {
        
    var listReceipe: [RecipeTypeModel] = []
    var listReceipeModel: [RecipeModel] = []
    var getListRecipeTypeSuccess: (([RecipeTypeModel])->())?
    
    public func readType(successBlock: (([RecipeTypeModel])->())?) {
            guard let xmlPath = Bundle.main.path(forResource: "recipetypes", ofType: "xml"),
                let data = try? Data(contentsOf: URL(fileURLWithPath: xmlPath))
            else { return }

            var options = AEXMLOptions()
            options.parserSettings.shouldProcessNamespaces = false
            options.parserSettings.shouldReportNamespacePrefixes = false
            options.parserSettings.shouldResolveExternalEntities = false
            
            do {
                let xmlDoc = try AEXMLDocument(xml: data, options: options)
                for child in xmlDoc.root.children {
                    print(child)
                    let model = RecipeTypeModel(element: child)
                    listReceipe.append(model)
                }
                
                if (successBlock != nil) {
                    successBlock!(listReceipe)
                }
            }
            catch {
                print("\(error)")
            }
    }
    
    public func readListRecipe(successBlock: (([RecipeModel])->())?) {
        let realm = try! Realm()
        let recipeList = realm.objects(RecipeModel.self)
        if (successBlock != nil) {
            successBlock!(Array(recipeList))
            listReceipeModel = Array(recipeList)
        }
            
        
    }
}
