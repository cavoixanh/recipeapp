//
//  ViewController.swift
//  RecipeApp
//
//  Created by Admin on 2/21/20.
//  Copyright © 2020 recipe. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class ListRecipeViewController: UIViewController {

    @IBOutlet private weak var picker: UIPickerView!
    @IBOutlet private weak var tableView: UITableView!
    
    var presenter = RecipeTypePresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        configureNavigation()
        presenter.readType { (listModel) in
            print(listModel.count)
            self.picker.reloadAllComponents()
        }
        
        presenter.readListRecipe { (listRecipeVC) in
            self.tableView.reloadData()
        }
        // Do any additional setup after loading the view.
    }
    
    private func configureTableView() {
        //tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func configureNavigation() {
        let addButton = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addRecipe))
        let categoryButton = UIBarButtonItem(title: "Category", style: .plain, target: self, action: #selector(listRecipeType(_:)))
        navigationItem.rightBarButtonItems = [addButton, categoryButton]
    }
    
    @objc private func addRecipe() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateRecipeVC") as! CreateRecipeVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func listRecipeType(_ sender: UIBarButtonItem) {
        let titles = presenter.listReceipe.map { $0.name } as! [String]
        ActionSheetStringPicker.show(withTitle: "Recipe Type", rows: titles, initialSelection: 0, doneBlock: { (picker, row, cell) in
            
        }, cancel: { (picker) in
            
        }, origin: sender)
    }
}

extension ListRecipeViewController: UIPickerViewDelegate {
    
}

extension ListRecipeViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return presenter.listReceipe.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let model = presenter.listReceipe[row]
        return model.name
    }
    
}

extension ListRecipeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.listReceipeModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RecipeCell.fileNameCell) as! RecipeCell
        cell.setupCell(model: presenter.listReceipeModel[indexPath.row])
        return cell
    }
}

