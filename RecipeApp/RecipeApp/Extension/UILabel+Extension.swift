//
//  UILabel+Extension.swift
//  F88
//
//  Created by Tuan Nguyen on 4/5/19.
//  Copyright © 2019 F88. All rights reserved.
//

import UIKit

extension UILabel {

    func setPrettyText(_ text: String) {
        let attributeString = text.getAttributeString(font: self.font ?? UIFont.systemFont(ofSize: 15),
                                                      color: self.textColor)
        attributeString.setLineSpacing(lineSpacing: 4.0, alignment: self.textAlignment)
        self.attributedText = attributeString
    }

    func getTrimmedText() -> String? {
        return self.text?.trimmingCharacters(in: .whitespaces)
    }

}
