//
//  UINavigationController+Extension.swift
//  F88
//
//  Created by Tuan Nguyen on 4/3/19.
//  Copyright © 2019 F88. All rights reserved.
//

import UIKit

extension UINavigationController {

    func setHiddenNavigationBar() {
        self.isNavigationBarHidden = true
        self.navigationBar.isTranslucent = false
    }

}
