//
//  UISwitch+Extension.swift
//  F88
//
//  Created by Tuan Nguyen on 5/28/19.
//  Copyright © 2019 F88. All rights reserved.
//

import UIKit

extension UISwitch {

    func resize() {
        let scale: CGFloat = 0.65
        self.transform = CGAffineTransform(scaleX: scale, y: scale)
    }

}
