//
//  UIPageControl+Extension.swift
//  F88
//
//  Created by Tuan Nguyen on 8/20/19.
//  Copyright © 2019 F88 Mobile. All rights reserved.
//

import UIKit

extension UIPageControl {
    
    func customPageControl(dotFillColor: UIColor, dotBorderColor: UIColor, dotBorderWidth: CGFloat) {
        for (pageIndex, dotView) in self.subviews.enumerated() {
            dotView.layer.cornerRadius = dotView.frame.size.height / 2
            if self.currentPage == pageIndex {
                dotView.backgroundColor = dotFillColor
            } else {
                dotView.backgroundColor = .clear
                dotView.layer.borderColor = dotBorderColor.cgColor
                dotView.layer.borderWidth = dotBorderWidth
            }
        }
    }
    
}
