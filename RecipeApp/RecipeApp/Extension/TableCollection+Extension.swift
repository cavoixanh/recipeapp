//
//  TableCollection+Extension.swift
//  F88
//
//  Created by Tuan Nguyen on 3/8/19.
//  Copyright © 2019 F88. All rights reserved.
//

import UIKit

extension UITableView {
    
    func registerCells(_ identifiers: String...) {
        for identifier in identifiers {
            self.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        }
    }

    func reloadDataNoAnimated() {
        UIView.setAnimationsEnabled(false)
        reloadData()
        UIView.setAnimationsEnabled(true)
    }

    func reloadSectionNoAnimated(_ sections: Int...) {
        UIView.setAnimationsEnabled(false)
        reloadSections(IndexSet(sections), with: .none)
        UIView.setAnimationsEnabled(true)
    }

    func reloadRowNoAnimated(_ indexPaths: IndexPath...) {
        UIView.setAnimationsEnabled(false)
        reloadRows(at: indexPaths, with: .none)
        UIView.setAnimationsEnabled(true)
    }

    func addIndicator() {
        let indicator = UIActivityIndicatorView(style: .gray)
        indicator.frame = CGRect(x: 00, y: 0, width: bounds.width, height: 44)
        indicator.startAnimating()
        tableHeaderView = indicator
    }

    func removeIndicator() {
        if let indicator = tableHeaderView as? UIActivityIndicatorView {
            indicator.stopAnimating()
            indicator.removeFromSuperview()
        }
        self.tableHeaderView = nil
    }

}

extension UICollectionView {
    
    func registerCells(_ identifiers: String...) {
        for identifier in identifiers {
            self.register(UINib(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
        }
    }

    func reloadDataNoAnimated() {
        UIView.setAnimationsEnabled(false)
        reloadData()
        UIView.setAnimationsEnabled(true)
    }

    func reloadSectionNoAnimated(_ sections: Int...) {
        UIView.setAnimationsEnabled(false)
        reloadSections(IndexSet(sections))
        UIView.setAnimationsEnabled(true)
    }

    func reloadRowNoAnimated(_ indexPaths: IndexPath...) {
        UIView.setAnimationsEnabled(false)
        reloadItems(at: indexPaths)
        UIView.setAnimationsEnabled(true)
    }

}

/*
    - Cần phải handle trong function 'scrollViewDidEndDragging'
    - Có thể xem example trong file 'NotificationsViewController'
 */
let kReloadDistance: CGFloat = 20
extension UIScrollView {

    func addPullToRefresh(isLoading: Bool, handle:(() -> Void)) {
        let offset = self.contentOffset
        guard offset.y < -kReloadDistance, !isLoading else {
            return
        }
        handle()
    }

    func addLoadMore(isVertical: Bool = true, isFullData: Bool, isEmptyData: Bool, isLoading: Bool, handle:(() -> Void)) {
        let offset = self.contentOffset
        let bounds = self.bounds
        let inset = self.contentInset
        let xPosition = offset.x + bounds.size.width - inset.right
        let yPosition = offset.y + bounds.size.height - inset.bottom
        let width = self.contentSize.width
        let height = self.contentSize.height
        let condition = isVertical ? yPosition > (height + kReloadDistance) : xPosition > (width + kReloadDistance)
        let customOffset = isVertical ? offset.y > 0 : offset.x > 0
        guard condition, customOffset, !isFullData, !isEmptyData, !isLoading else {
            return
        }
        handle()
    }

}
