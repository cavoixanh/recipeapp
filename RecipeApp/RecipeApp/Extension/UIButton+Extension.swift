//
//  UIButton+Extension.swift
//  F88
//
//  Created by Tuan Nguyen on 3/27/19.
//  Copyright © 2019 F88. All rights reserved.
//

import UIKit

extension UIButton {

    func setState(isEnable: Bool) {
        self.isUserInteractionEnabled = isEnable
    }
    
}
