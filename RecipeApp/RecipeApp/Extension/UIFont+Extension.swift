//
//  UIFont+Extension.swift
//  F88
//
//  Created by Tuan Nguyen on 4/2/19.
//  Copyright © 2019 F88. All rights reserved.
//

import UIKit

enum UTMHelveStyle: String {

    case regular = "UTM-Helve"
    case italic = "UTMHelve-Italic"
    case boldItalic = "UTMHelve-BoldItalic"
    case bold = "UTMHelveBold"

}

// MARK: - Font Montserrat Style
enum RobotoStyle {
    
    case black
    case blackItalic
    case bold
    case boldCondensed
    case boldCondensedItalic
    case boldItalic
    case condensed
    case condensedItalic
    case italic
    case light
    case lightItalic
    case medium
    case mediumItalic
    case regular
    case thin
    case thinItalic
    
    func toString() -> String {
        return "Roboto-\(String(describing: self).capitalized)"
    }
    
}

extension UIFont {

    class func getUTMHelveFont(size: CGFloat, style: UTMHelveStyle) -> UIFont {
        guard let font = UIFont(name: style.rawValue, size: size) else {
            return UIFont.systemFont(ofSize: size)
        }
        return font
    }

    class func getRobotoFont(size: CGFloat, style: RobotoStyle) -> UIFont {
        guard let font = UIFont(name: style.toString(), size: size) else {
            return UIFont.systemFont(ofSize: size)
        }
        return font
    }

    func getMultiDeviceFont() -> UIFont? {
        /*
            README
            - Vì trên file design là bên design cung cấp cho iPhone XSMax nên app đang dùng hết dimension cho XSMax trở lên nên
              vì thế những iPhone màn hình bé hơn sẽ hiển thị rất xấu
         */
        var newSize = pointSize
        switch DetectDevice.type {
        case .iPhone4, .iPhone5:
            newSize = pointSize - 2
        case .iPhone6, .iPhoneX:
            break
        case .iPhone6Plus, .iPhoneXsMax:
            newSize = pointSize + 1
        default:
            break
        }
        return UIFont(name: fontName, size: newSize)
    }
    
    func getNewFont(size: CGFloat) -> UIFont? {
        return UIFont(name: fontName, size: pointSize + size)
    }

}

// MARK: - Multi Font Size
extension UILabel {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        font = font.getMultiDeviceFont()
    }
    
}

extension UIButton {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel?.font = titleLabel?.font.getMultiDeviceFont()
    }
    
}

extension UITextField {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        font = font?.getMultiDeviceFont()
    }
    
}

extension UITextView {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        font = font?.getMultiDeviceFont()
    }
    
}
