//
//  SDWebImage+Extension.swift
//  F88
//
//  Created by Tuan Nguyen on 3/25/19.
//  Copyright © 2019 F88. All rights reserved.
//

import UIKit
import Kingfisher

// swiftlint:disable nesting line_length
extension UIImageView {

    func loadImage(urlString: String?, placehoderImage: UIImage = #imageLiteral(resourceName: "no_image")) {
        guard let url = URL(string: urlString ?? "") else {
            return
        }
        self.kf.indicatorType = .activity
        self.kf.setImage(with: url, placeholder: nil, options: [.scaleFactor(UIScreen.main.scale), .transition(.fade(1)), .cacheOriginalImage], progressBlock: nil) { (result: Result<RetrieveImageResult, KingfisherError>) in
            switch result {
            case .success:
                break
            case .failure:
                self.image = placehoderImage
            }
        }
    }

    func changeImageTintColor(_ color: UIColor) {
        self.image = self.image?.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }

}

extension UIImage {

    func reductionQuality(_ quality: CGFloat = 0.5) -> Data? {
        guard let data = self.jpegData(compressionQuality: quality) else {
            return nil
        }
        return data
    }

}
