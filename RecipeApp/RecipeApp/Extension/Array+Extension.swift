//
//  Array+Extension.swift
//  F88
//
//  Created by Tuan Nguyen on 7/4/19.
//  Copyright © 2019 F88. All rights reserved.
//

import Foundation

extension Array {

    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }

}
