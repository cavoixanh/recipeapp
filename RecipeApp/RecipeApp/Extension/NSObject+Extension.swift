//
//  NSObject+Extension.swift
//  Common
//
//  Created by Tuấn Bờm on 5/4/18.
//  Copyright © 2018 Tuấn Bờm. All rights reserved.
//

import Foundation

extension NSObject {
	
	var fileName: String {
		return String(describing: type(of: self))
	}
	
    class var fileName: String {
        return String(describing: self)
    }
}
