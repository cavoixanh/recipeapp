//
//  UITextField+Extension.swift
//  F88
//
//  Created by Tuan Nguyen on 3/8/19.
//  Copyright © 2019 F88. All rights reserved.
//

import UIKit

extension UITextField {

    func changePlacehoderColor(_ color: UIColor) {
        self.setValue(color, forKeyPath: "_placeholderLabel.textColor")
    }

    func getTrimmedText() -> String? {
        return self.text?.trimmingCharacters(in: .whitespaces)
    }
    
    @IBInspectable
    var placehoderColor: UIColor? {
        get {
            return nil
        }
        
        set {
            if let color = newValue {
                self.setValue(color, forKeyPath: "_placeholderLabel.textColor")
            }
        }
    }

}
