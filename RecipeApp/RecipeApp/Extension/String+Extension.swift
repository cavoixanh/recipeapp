//
//  String+Extension.swift
//  F88
//
//  Created by Tuan Nguyen on 3/8/19.
//  Copyright © 2019 F88. All rights reserved.
//

import UIKit
import Foundation

extension String {

    func smartContains(_ other: String) -> Bool {
        let newString = self.replacingOccurrences(of: " ", with: "").lowercased()
        let newOtherString = other.replacingOccurrences(of: " ", with: "").lowercased()
        return newString.contains(newOtherString)
    }

    func getWidthString(_ font: UIFont, mHeight: CGFloat, maxWidth: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: maxWidth, height: mHeight)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.width
    }

    func getHeightString(_ font: UIFont, maxHeight: CGFloat, mWidth: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: mWidth, height: maxHeight)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }

    func getDate(format: DateFormat) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        return dateFormatter.date(from: self) ?? nil
    }

    func getStringDate(oldFormat: DateFormat, newFormat: DateFormat) -> String? {
        guard let date = self.getDate(format: oldFormat) else {
            return nil
        }
        return date.getString(format: newFormat)
    }

    func getStrikeThrough(font: UIFont, color: UIColor) -> NSAttributedString {
        let attributeString = getAttributeString(font: font, color: color)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle,
                                     value: 1,
                                     range: NSRange(location: 0, length: attributeString.length))
        return attributeString
    }

    func getUnderLine(font: UIFont, color: UIColor) -> NSAttributedString {
        let attributeString = getAttributeString(font: font, color: color)
        attributeString.addAttribute(NSAttributedString.Key.underlineStyle,
                                     value: 1, range: NSRange(location: 0, length: attributeString.length))
        return attributeString
    }

    func getAttributeString(font: UIFont, color: UIColor) -> NSMutableAttributedString {
        let attribute = [NSAttributedString.Key.font: font,
                         NSAttributedString.Key.foregroundColor: color]
        let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: self)
        attributeString.addAttributes(attribute, range: NSRange(location: 0, length: attributeString.length))
        return attributeString
    }

    func convertToHtmlString() -> NSAttributedString? {
        guard let data = data(using: .utf8) else {
            return nil
        }
        do {
            return try NSMutableAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            return nil
        }
    }

    func getCleanDouble() -> Double? {
        guard let originDouble = Double(self) else {
            return nil
        }
        let cleanString = String(format: "%.1f", originDouble)
        return Double(cleanString)
    }

    func removeStartEndWhiteSpaces() -> String {
        return self.trimmingCharacters(in: .whitespaces)
    }

    func removeAllWhiteSpaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }

    func splitedBy(length: Int) -> [String] {
        var result = [String]()
        for index in stride(from: 0, to: self.count, by: length) {
            let endIndex = self.index(self.endIndex, offsetBy: -index)
            let startIndex = self.index(endIndex, offsetBy: -length, limitedBy: self.startIndex) ?? self.startIndex
            result.append(String(self[startIndex..<endIndex]))
        }
        return result.reversed()
    }
    
    func random(length: Int) -> String {
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""
        
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }
    
    func stringByAddingPercentEncodingForFormData(plusForSpace: Bool=false) -> String {
        let firstCollection = ["đ", "ê", "ô", "ơ", "â", "ă", "ư"]
        let replaceCollection = ["d", "e", "o", "o", "a", "a", "u"]
        var string = self
        for ii in 0..<firstCollection.count {
            string = string.replacingOccurrences(of: firstCollection[ii], with: replaceCollection[ii])
        }
        
        if plusForSpace {
            string = string.replacingOccurrences(of: " ", with: "%20")
        }
        
        return string
    }
    
    func convertHtml() -> NSAttributedString {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    
}

extension NSMutableAttributedString {

    func setLineSpacing(lineSpacing: CGFloat = 5.0, alignment: NSTextAlignment) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.alignment = alignment
        // Line spacing attribute
        self.addAttribute(NSAttributedString.Key.paragraphStyle,
                          value: paragraphStyle,
                          range: NSRange(location: 0, length: self.length))
    }

}
