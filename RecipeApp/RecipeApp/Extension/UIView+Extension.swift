//
//  UIView+Extension.swift
//  F88
//
//  Created by Tuan Nguyen on 4/19/19.
//  Copyright © 2019 F88. All rights reserved.
//

import UIKit

enum PositionViewType {

    case left, right, top, bottom, onlyLeft, onlyTopLeft, onlyTopRight

}

extension UIView {

    class func loadXib<T: UIView>() -> T {
        guard let view = Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)?.first as? T else {
            return T()
        }
        return view
    }

    func setRounded(radius: CGFloat, type: PositionViewType) {
//        self.clipsToBounds = true
        if #available(iOS 11.0, *) {
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = convertCornerType(type).lastestType
        } else {
            let path = UIBezierPath(roundedRect: self.bounds,
                                    byRoundingCorners: convertCornerType(type).earlierType,
                                    cornerRadii: CGSize(width: radius, height: radius))
            let maskLayer = CAShapeLayer()
            maskLayer.path = path.cgPath
            self.layer.mask = maskLayer
        }
    }
    
    func removeRounded() {
        self.layer.cornerRadius = 0
        self.clipsToBounds = true
    }

    private func convertCornerType(_ type: PositionViewType) -> (lastestType: CACornerMask, earlierType: UIRectCorner) {
        var lastestType: CACornerMask = .layerMinXMaxYCorner
        var earlierTypes: UIRectCorner = .bottomLeft
        switch type {
        case .left:
            lastestType = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
            earlierTypes = [.topLeft, .bottomLeft]
        case .right:
            lastestType = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
            earlierTypes = [.topRight, .bottomRight]
        case .bottom:
            lastestType = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            earlierTypes = [.bottomLeft, .bottomRight]
        case .top:
            lastestType = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            earlierTypes = [.topLeft, .topRight]
        case .onlyLeft:
            lastestType = [.layerMinXMaxYCorner]
            earlierTypes = [.bottomLeft]
        case .onlyTopLeft:
            lastestType = [.layerMinXMinYCorner]
            earlierTypes = [.topLeft]
        case .onlyTopRight:
            lastestType = [.layerMaxXMinYCorner]
            earlierTypes = [.topRight]
        }
        return (lastestType, earlierTypes)
    }

    func moveTransition(_ duration: CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.moveIn
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
    
    // MARK: - Customize UIView
    @IBInspectable
    var isHiden: Bool {
        get {
            return isHidden
        }
        set {
            if newValue {
                self.isHidden = newValue
            }
        }
    }
    
    @IBInspectable
    var halfCorner: Bool {
        get {
            return true
        }
        
        set {
            if newValue {
                Thread.mainAfter(0.1) { [weak self] in
                    guard let wSelf = self else {
                        return
                    }
                    wSelf.layer.cornerRadius = wSelf.frame.height / 2
                }
            }
        }
    }
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }

}
