//
//  String+Validation.swift
//  F88
//
//  Created by Tuan Nguyen on 3/8/19.
//  Copyright © 2019 F88. All rights reserved.
//

import Foundation

enum Regex: String {

    case password = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{2,}$"
    case phoneNumber = "0[0-9]{6,15}$"

}

extension NSRegularExpression {

    func matches(_ string: String) -> Bool {
        let range = NSRange(location: 0, length: string.utf16.count)
        return firstMatch(in: string, options: [], range: range) != nil
    }

}

extension String {

    func validateWith(regex: Regex) -> Bool {
        guard let expression = try? NSRegularExpression(pattern: regex.rawValue) else {
            return false
        }
        return expression.matches(self)
    }

    func validateIsEmpty() -> Bool {
        return self.trimmingCharacters(in: .whitespaces).isEmpty
    }

    func validateWithRange(minCharacter: Int, maxCharacter: Int) -> Bool {
        let numberChars = self.trimmingCharacters(in: .whitespaces).count
        return numberChars >= minCharacter && numberChars <= maxCharacter
    }

}
