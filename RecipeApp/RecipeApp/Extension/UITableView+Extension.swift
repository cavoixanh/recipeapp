//
//  UITableView+Extension.swift
//  F88
//
//  Created by Thiên Vương on 9/25/19.
//  Copyright © 2019 F88 Mobile. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
    
}
