//
//  Date+Extension.swift
//  F88
//
//  Created by Tuan Nguyen on 3/8/19.
//  Copyright © 2019 F88. All rights reserved.
//

import Foundation

// MARK: - DateFormat
enum DateFormat: String {

    case apiStyle = "yyyy-MM-dd HH:mm:ss ZZZZZ"
    case fullStyle = "yyyy-MM-dd HH:mm:ss"
    case dateStyle = "dd-MM-yyyy"
    case logStyle = "yyyyMMddHHmmss"

}

extension Date {

    func getString(format: DateFormat) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        return dateFormatter.string(from: self)
    }

    func getAge() -> Int {
        let timeInterval = self.timeIntervalSinceNow
        let age = abs(Int(timeInterval / 31556926.0))
        return age
    }

    func getAfter(type: Calendar.Component, value: Int) -> Date? { 
        return Calendar.current.date(byAdding: type, value: value, to: self)
    }

}
