//
//  NotificationCenter.swift
//  F88
//
//  Created by Tuan Nguyen on 3/21/19.
//  Copyright © 2019 F88. All rights reserved.
//

import Foundation

extension Notification.Name {

    static let userLogout = Notification.Name("UserLogout")
    static let fastLoan = Notification.Name("FastLoan")
    static let backHome = Notification.Name("BackHome")
    static let transaction = Notification.Name("Transaction")
    static let profile = Notification.Name("Profile")
    static let notification = Notification.Name("Notification")
    static let notificationCount = Notification.Name("notificationCount")

}
