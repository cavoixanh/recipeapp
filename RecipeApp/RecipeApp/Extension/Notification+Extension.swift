//
//  Notification+Extension.swift
//  F88
//
//  Created by Thiên Vương on 9/11/19.
//  Copyright © 2019 F88 Mobile. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let fcmRecivedNoti = Notification.Name("fcmRecivedNoti")
}
