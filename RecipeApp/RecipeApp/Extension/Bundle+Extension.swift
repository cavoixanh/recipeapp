//
//  Bundle+Extension.swift
//  F88
//
//  Created by Thiên Vương on 10/29/19.
//  Copyright © 2019 F88 Mobile. All rights reserved.
//

import Foundation

extension Bundle {
    
    var osType: String {
        return "IOS"
    }
    
    var appName: String? {
        return infoDictionary?["CFBundleName"] as? String
    }

    var bundleId: String {
        return bundleIdentifier!
    }

    var versionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }

    var buildNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
    
}
