//
//  Float+Extension.swift
//  F88
//
//  Created by The Truong on 2/9/20.
//  Copyright © 2020 F88 Mobile. All rights reserved.
//

import UIKit

extension CGFloat {
    func getMultiDeviceScale() -> CGFloat {
        /*
            README
            - Dùng cho màn hình home, set inset của scrollView
         */
        switch DetectDevice.type {
        case .iPhone4, .iPhone5:
            return self*3
        case .iPhone6:
            return self*1.9
        case .iPhoneX:
            return self
        case .iPhone6Plus:
            return self
        case .iPhoneXsMax:
            return self*0.2
        default:
            return self
        }
    }
}
