//
//  RecipeModel.swift
//  RecipeApp
//
//  Created by Admin on 2/22/20.
//  Copyright © 2020 recipe. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class RecipeModel: Object {

    @objc dynamic var type = 0
    @objc dynamic var name: String = ""
    @objc dynamic var content: String = ""
    
}
