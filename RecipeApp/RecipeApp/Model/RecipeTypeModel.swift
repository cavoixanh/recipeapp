//
//  RecipeModel.swift
//  RecipeApp
//
//  Created by Admin on 2/21/20.
//  Copyright © 2020 recipe. All rights reserved.
//

import UIKit
import AEXML

struct RecipeTypeModel {

    var id: Int?
    var name: String?
    
    init(element: AEXMLElement) {
        //print(element.attributes)
        self.id = Int(element.attributes["idd"] ?? "0")
        self.name = element.value
    }
}
